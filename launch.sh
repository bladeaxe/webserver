#!bin/bash

# variables to pass

terra_folder="./terraform"

parameters_count=5
environment=$1
access_key=$2
secret_key=$3
key_pair=$4
region=$5


function usage {
    
    echo "Usage:" 
    echo "To run script you need to pass variables:"
    echo "$0 <environment> <access_key> <secret_key> <key_pair> <region>"
    echo "Avaliable environments: SANDBOX"
    echo ""
    
}

if [ "$#" -ne "${parameters_count}" ]; then
    usage
    exit 1
fi

cd $terra_folder || return

echo "terraform apply -var "environmen="$environment"" -var "access_key="$access_key"" -var "secret_key="$secret_key"" -var "key_pair="$key_pair"" -var "region="$region"""
