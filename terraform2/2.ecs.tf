resource "aws_ecr_repository" "openjobs_app" {
  name = "webapp"

depends_on = ["module.vpc"]

} 

resource "aws_ecs_cluster" "cluster" {
  name = "webapp-ecs-cluster"

depends_on = ["module.vpc"]

}


