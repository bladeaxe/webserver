variable "access_key" {
  description = "AWS access key"
  default     = ""
}

variable "secret_key" {
  description = "AWS secret key"
  default     = ""
}

variable "region" {
  description = "Region to launch cfstack"
  default     = "us-east-1"
}
